# Analog

A digital gallery for analog photography by the designer Rayana Verissimo.

## To-do list
* [ ] A real README.md
  * [ ] License
  * [ ] Credits
  * [ ] Links
* [ ] Contributing.md
* [ ] Clean-up Sass to follow [The 7-1 Pattern](https://sass-guidelin.es/#architecture)
* [ ] Make favicon cacheable
* [ ] Optimize images
* [ ] Add google analytics
* [ ] Add hotjar
* [ ] Include last updated date
* [ ] Add page title to website head
