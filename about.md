---
layout: page
title: About
permalink: /about/
---
<h2>About</h2>
As most things go, photos make me reconnect with my childhood, to a time when the camera was sacred and saved for special occasions. There was a limited number of photos we could get per roll - usually 12... 24 if my mom was feeling adventurous - and I had to wait until the roll was developed to see if I managed to keep my eyes open in every photo. Usually I would not. And more commonly, the negatives would came out black. These were the times.

To this day I am addicted to making photos, and whenever I have the chance I grab an analog camera.

I enjoy randomness. You probably will notice that browsing through this website -- I will capture on film anything that captures my eyes. I hope to stay unpretentious.

I am a designer, currently based in Amsterdam, The Netherlands.

<h3>About this website</h3>
This website is produced on a MacBook Pro and coded in [Atom](https://atom.io/). Type is set in Courier by Howard Kettler. It uses [GitLab Pages](#) for web hosting and the code is freely available in a [GitLab repository](). I welcome any comments, suggestions, and corrections to the code and content. 

Pages are structured using [HTML5](http://en.wikipedia.org/wiki/HTML5) and styled via [CSS files](http://www.w3.org/Style/CSS/). This is mostly an experiment with [CSS Grid Layout](https://en.wikipedia.org/wiki/CSS_grid_layout) and the full experience might not be supported by older browsers.
